Rails.application.routes.draw do
  devise_for :users

  resources :users
  post 'users/create_user' => 'users#create', as: :my_create_user 
  patch 'users/update_user/:id' => 'users#update', as: :my_update_user 

  get 'welcome/index'

  resources :articles do
  	resources :comments
  end

  root 'welcome#index'
end
