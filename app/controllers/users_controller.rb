class UsersController < ApplicationController

	before_action :authenticate_user!

	def index
		@users = User.all
	end

	def new
		@user = User.new
	end

	def edit
  		@user = User.find(params[:id])
	end

	def create
		#binding.pry
		@user = User.new(user_params)
		if @user.save
		  redirect_to @user
		else
		  render 'new'
		end
	end

	def update
		#binding.pry
  		@user = User.find(params[:id])
  		if @user.update(user_params)
    		redirect_to @user
  		else
    		render 'edit'
  		end
	end

	def show
		@user = User.find(params[:id])
	end

	def destroy
  		@user = User.find(params[:id])
  		@user.destroy
  		redirect_to users_path
	end

	private
	  def user_params
	    params.require(:user).permit(:email, :item_ids => [])
	  end
end
